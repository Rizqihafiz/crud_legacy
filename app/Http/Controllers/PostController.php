<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Services\PostService;
use PhpParser\Node\Stmt\TryCatch;

use function PHPUnit\Framework\isNull;

class PostController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function findAll(){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->postService->getAll();

        }
        catch(Exception $e){
                $result = [
                    'status' => 500,
                    'message' => $e->getMessage()
                ];
        }
        //return response()->json($result,$result['status']);
        return view('post.index-post',['posts' => $result['data']]);
    }

    public function getById($id){
        $post = $this->postService->findById($id);
        return view('post.edit-post',['post' => $post]);
    }

    public function storeOrUpdate(Request $request,$id = null){
        $data = $request->only([
        'title',
        'description'
        ]);

        $result = ['status' => 201];
        try{
            if(!isNull($id) or $id != ""){
                $result['data'] = $this->postService->update($id,$data);
                $result['message'] = 'data with id '.$id.' updated';
            }
            else{
            $result['data'] = $this->postService->saveData($data);
            $result['message'] = 'data with id '.$result['data']['id']. ' created';
        }
        }
        catch(Exception $e){
            $result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }
        return redirect('/post')->with('message',''.$result['message']);
    }

    public function delete($id){
        if ($this->postService->deleteService($id)){
            return redirect('/post')->with('message','Data with id '.$id.' deleted');
        }
        else {
            echo "delete failed";
        }
    }



}
