<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository
{
    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function findAll(){

        return $this->post->get();
    }

    public function save($data){
        $post = new $this->post;
        $post->title = $data['title'];
        $post->description = $data['description'];

        $post->save();
        return $post->fresh();
    }

    public function update($id,$data){
        $post = Post::Find($id);
        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->save();
        return $post;
    }

    public function delete($id){
        return Post::Find($id)->delete();
    }

    public function findById($id){
        return Post::Find($id);
    }
}
