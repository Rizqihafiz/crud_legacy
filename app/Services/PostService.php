<?php
namespace App\Services;

use App\Repositories\PostRepository;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class PostService{

    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getAll(){

        return $this->postRepository->findAll();
    }

    public function saveData($data){
        $validator = Validator::make($data,[
            'title' => 'required',
            'description' => 'required',
        ]);
        if($validator->fails()){
            throw new InvalidArgumentException($validator->errors()->first());
        }
        $result = $this->postRepository->save($data);
        return $result;
    }

    public function update($id,$data){
        $validator = Validator::make($data,[
            'title' => 'required',
            'description' => 'required',
        ]);
        if($validator->fails()){
            throw new InvalidArgumentException($validator->errors()->first());
        }
        $result = $this->postRepository->update($id,$data);
        return $result;
    }

    public function deleteService($id){
        return $this->postRepository->delete($id);
    }

    public function findById($id){
        return $this->postRepository->findById($id);
    }
}
