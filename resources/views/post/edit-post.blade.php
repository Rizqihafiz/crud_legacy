@extends('layouts.master')
@section('title' ,'Edit')
@section('main_content')
    <div class="row justify-content-center">
        <div class="col-6">
            <h2 class="text-center">Edit Post</h2>
            <hr>
            <form action="{{route('post.update',$post->id)}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <input type="text" class="form-control" name="title"value="{{$post->title}}" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="description" value="{{$post->description}}" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-success">Save Data</button>
                </div>
            </form>
            <a href ="{{route('post.index')}}" class="btn btn-block btn-info">Back</a>
        </div>
    </div>
@stop
