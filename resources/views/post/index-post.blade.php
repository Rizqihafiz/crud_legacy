@extends('layouts.master')
@section('title' ,'Home')
@section('main_content')
    <div class="row">
        <div class="col-12">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Add New Data
            </button>
            <hr>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @forelse($posts as $post)
                    <tr>

                        <td>{{$post->id}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->description}}</td>
                        <td>
                            <a href="{{route('post.view',$post->id)}}" class="btn btn-sm btn-info">Edit</a>
                            <a href="{{route('post.delete',$post->id)}}" class="btn btn-sm btn-danger">Trash</a>
                        </td>
                    </tr>
                @empty
                @endforelse
                </tbody>
            </table>
            <hr>
                   </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('post.save')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" placeholder="Enter Title" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="description" placeholder="Description" required>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-success">Add New Data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
