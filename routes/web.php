<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'post','as' => 'post.'],function(){
Route::get('/',[PostController::class,'findAll'])->name('index');
Route::post('save/',[PostController::class,'storeOrUpdate'])->name('save');
Route::put('update/{id}',[PostController::class,'storeOrUpdate'])->name('update');
Route::get('delete/{id}',[PostController::class,'delete'])->name('delete');
Route::get('/{id}',[PostController::class,'getById'])->name('view');
});
